DEV: https://spontaneous-babka-74b6b0.netlify.app/

# My Ecommerce App
[![Netlify Status](https://api.netlify.com/api/v1/badges/909c7a38-f4c2-4001-9f9b-c16c852bae84/deploy-status)](https://app.netlify.com/sites/spontaneous-babka-74b6b0/deploys)
This is my project description.

- Writting...
- Writting...
- Writting...

## Usage
### Prerequisites
Make sure you have the following installed:
```sh
node.js
yarn
```
## Installation
Clone this repository and install the dependencies:

Install the dependencies and devDependencies and start the server.

```sh
git clone https://github.com/myrepo
cd myrepo
yarn install
```
Then you can run the project:

```sh
yarn dev
```
## Commit Message Format
Atomic commits are recommended. All commit messages must comply with the conventional rules of commit messages.
```bash
type(scope?): subject
```
Examples (the template):
```bash
feat(build): add webpack config
```
```bash
fix(docs): fix typo in modal example
```
##### Types
Must be one of the following:

- build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- ci: Changes to our CI configuration files and scripts (example scopes: Gitlab CI, Circle, BrowserStack, SauceLabs)
- chore: add something without touching production code (Eg: update npm dependencies)
- docs: Documentation only changes
- feat: A new mize
- fix: A bug fix
- perf: A code change that improves performance
- refactor: A code change that neither fixes a bug nor adds a feature
- revert: Reverts a previous commit
- style: Changes that do not affect the meaning of the code (Eg: adding white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests


##### Scope
The scope could be anything specifying a place of the commit change. For example core, controller, config etc.

