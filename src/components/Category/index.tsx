import classNames from 'classnames';
import React, { useState } from 'react';
import { IoIosArrowDown, IoIosArrowForward } from 'react-icons/io';
import { Link, useLocation } from 'react-router-dom';

interface ICategoryItem {
  id: number;
  title: string;
  icon: JSX.Element;
  subCategory: {
    title: string;
    label: 'new' | 'normal' | 'hot';
    url: string;
    path: string;
  }[];
  isChecked?: boolean;
}
export interface ICategoryData {
  id: number;
  title: string;
  data: ICategoryItem[];
}

function CategoryItem({ icon, title, subCategory, isChecked }: Omit<ICategoryItem, 'id'>) {
  const location = useLocation();
  const [isShowCategoryList, setIsShowCategoryList] = useState(isChecked);

  const onToggleShowCategories = () => {
    setIsShowCategoryList(!isShowCategoryList);
  };

  return (
    <li>
      <div
        aria-hidden="true"
        className={classNames('flex items-center text-sm hover:text-slate-50 hover:cursor-pointer mb-5', {
          'text-slate-50': isShowCategoryList,
          'text-slate-50/[0.6]': !isShowCategoryList,
        })}
        onClick={onToggleShowCategories}
      >
        {icon}
        <p className="text-[15px]">{title}</p>
        {isShowCategoryList ? <IoIosArrowDown className="ml-auto" /> : <IoIosArrowForward className="ml-auto" />}
      </div>
      <ul
        className={classNames(`category-list-child pl-6 list-disc flex flex-col gap-2 mb-3`, {
          'i-show': isShowCategoryList,
        })}
      >
        {subCategory.map((item) => {
          const pathName = location.pathname.slice(6);
          const isActive = item.path === pathName ? 'text-slate-50' : 'text-slate-50/[0.6]';
          return (
            <li key={item.title}>
              <Link to={`/admin${item.path}`} className={`text-[13px] ${isActive} hover:text-slate-50`}>
                {item.title}
              </Link>
            </li>
          );
        })}
      </ul>
    </li>
  );
}

function Category({ title, data }: Pick<ICategoryData, 'title' | 'data'>) {
  const location = useLocation();
  const pathName = location.pathname.slice(6);
  return (
    <div className="mb-2">
      <p className="mb-3 text-xs text-gray-100 uppercase">{title}</p>
      <ul className="pl-2">
        {data.map((item) => {
          const isChecked = item.subCategory.some((i) => i.path === pathName);
          return (
            <CategoryItem
              key={item.id}
              icon={item.icon}
              title={item.title}
              subCategory={item.subCategory}
              isChecked={isChecked}
            />
          );
        })}
      </ul>
    </div>
  );
}

export default Category;
