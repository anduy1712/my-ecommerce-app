import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../Header';

interface LayoutProps extends React.PropsWithChildren {
  marginTop?: string;
}

function Layout({ marginTop = 'mt-[110px]' }: LayoutProps) {
  return (
    <>
      <Header />
      <div className={`${marginTop} bg-slate-100`}>
        <div className="md:container md:mx-auto">
          <Outlet />
        </div>
      </div>
      <div className="footer">footer content</div>
    </>
  );
}

export default Layout;
