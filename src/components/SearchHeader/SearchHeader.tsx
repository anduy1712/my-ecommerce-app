import React from 'react';
import SearchIcon from 'src/assets/icons/SearchIcon';

function SearchHeader() {
  return (
    <div className="flex justify-between bg-white rounded-sm shadow-sm">
      <input
        className="w-full ml-2 text-sm text-black outline-none "
        placeholder="Đăng ký và nhận voucher bạn mới đến 70K"
        type="text"
      />
      <a href="/" className="px-5 py-2 mx-1 my-1 rounded-sm bg-nc-primary tex">
        <SearchIcon size={18} color="white" />
      </a>
    </div>
  );
}

export default SearchHeader;
