import React from 'react';
import LogoIcon from 'src/assets/icons/LogoIcon';
import { Link } from 'react-router-dom';
import { AiOutlineShoppingCart, AiFillInstagram, AiOutlineQuestionCircle } from 'react-icons/ai';
import { IoLogoFacebook } from 'react-icons/io';
import { BsBell } from 'react-icons/bs';
import { MdOutlineLanguage } from 'react-icons/md';
import Tippy from '@tippyjs/react';

import SearchHeader from '../SearchHeader/SearchHeader';

function Notification() {
  return (
    <div className="w-[450px] bg-white border border-slate-200">
      <div className="flex flex-col items-center justify-center w-full py-16">
        <img
          className="w-[20%]"
          src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/99e561e3944805a023e87a81d4869600.png"
          alt="notification_image"
        />
        <p>Đâng nhập để xem thông báo</p>
      </div>
      <div className="flex w-full py-2 bg-zinc-100">
        <Link to="/" className="w-1/2 text-center">
          Đăng Ký
        </Link>
        <Link to="/" className="w-1/2 text-center">
          Đăng Nhập
        </Link>
      </div>
    </div>
  );
}

function Header() {
  return (
    <header className="fixed top-0 left-0 right-0 z-50 text-white bg-gradient-to-b from-nc-primary to-nc-secondary">
      <div className="md:container md:mx-auto">
        <div className="flex justify-between py-1">
          <ul className="flex items-center gap-5">
            <li className="flex items-center h-full hover:brightness-90">
              <a
                href="/"
                className="relative text-xs font-light after:absolute after:bg-white after:w-[0.2px] after:h-[.9375rem] after:top-2/4 after:right-[-10px] after:-translate-y-1/2"
              >
                Kênh Người Bán
              </a>
            </li>
            <li className="flex items-center h-full hover:brightness-90">
              <a
                href="/"
                className="relative text-xs font-light after:absolute after:bg-white after:w-[0.2px] after:h-[.9375rem] after:top-2/4 after:right-[-10px] after:-translate-y-1/2"
              >
                Trở Thành Người Bán Shopee
              </a>
            </li>
            <li className="flex items-center h-full hover:brightness-90">
              <a
                href="/"
                className="relative text-xs font-light after:absolute after:bg-white after:w-[0.2px] after:h-[.9375rem] after:top-2/4 after:right-[-10px] after:-translate-y-1/2"
              >
                Tải Ứng Dụng
              </a>
            </li>
            <li className="flex items-center h-full hover:brightness-90">
              <Link to="/" className="relative mr-1 text-xs font-light">
                Kết Nối
              </Link>
              <Link to="/">
                <IoLogoFacebook />
              </Link>
              <Link to="/">
                <AiFillInstagram />
              </Link>
            </li>
          </ul>
          <ul className="flex gap-5">
            <li className="flex items-center h-full">
              <Tippy interactive animation="scale" content={<Notification />}>
                <div className="flex hover:cursor-pointer hover:brightness-90 ">
                  <BsBell className="mr-1" />
                  <p className="relative text-xs font-light after:absolute after:bg-white after:w-[0.2px] after:h-[.9375rem] after:top-2/4 after:right-[-10px] after:-translate-y-1/2">
                    Thông báo
                  </p>
                </div>
              </Tippy>
            </li>
            <li className="flex items-center h-full hover:brightness-90">
              <Link to="/" className="mr-1">
                <AiOutlineQuestionCircle />
              </Link>
              <a
                href="/"
                className="relative text-xs font-light after:absolute after:bg-white after:w-[0.2px] after:h-[.9375rem] after:top-2/4 after:right-[-10px] after:-translate-y-1/2"
              >
                Hỗ Trợ
              </a>
            </li>
            <li className="flex items-center h-full hover:brightness-90">
              <Link to="/" className="mr-1">
                <MdOutlineLanguage />
              </Link>
              <a
                href="/"
                className="relative text-xs font-light after:absolute after:bg-white after:w-[0.2px] after:h-[.9375rem] after:top-2/4 after:right-[-10px] after:-translate-y-1/2"
              >
                Tiếng Việt
              </a>
            </li>
            <li className="flex items-center h-full hover:brightness-90">
              <a
                href="/"
                className="relative text-xs font-bold after:absolute after:bg-white after:w-[0.2px] after:h-[.9375rem] after:top-2/4 after:right-[-10px] after:-translate-y-1/2"
              >
                Đăng Ký
              </a>
            </li>
            <li className="flex items-center h-full hover:brightness-90">
              <a href="/" className="relative text-xs font-bold ">
                Đâng Nhập
              </a>
            </li>
          </ul>
        </div>
        <div className="flex items-start my-3 ">
          <Link className="mr-5" to="/">
            <LogoIcon />
          </Link>
          <div className="w-[70%] mr-5">
            <SearchHeader />
            <ul className="flex items-center mt-1 text-xs font-light gap-4">
              <li>Áo Khoác</li>
              <li>Dép</li>
              <li>LEGO</li>
              <li>Túi Xách Nữ</li>
            </ul>
          </div>
          <div className="px-4 ">
            <Link to="/">
              <AiOutlineShoppingCart size={40} />
            </Link>
          </div>
        </div>
        <div />
      </div>
    </header>
  );
}

export default Header;
