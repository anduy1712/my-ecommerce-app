import React, { PropsWithChildren } from 'react';
import classNames from 'classnames';

interface Props extends PropsWithChildren {
  background?: string;
  color?: string;
  fontSize?: string;
  icon?: JSX.Element;
  className?: string;
}

function Button({
  background = 'bg-[#dbecf0]',
  color = 'text-[#0ab39c]',
  fontSize = 'text-[12px]',
  icon,
  children,
  className,
}: Props) {
  const borderColor: { [key: string]: string } = {
    'bg-[#dbecf0]': 'border-nc-primary',
    'bg-[#ffeee8]': 'border-nc-primary',
    'bg-[#e0ecef]': 'border-[#e0ecef]',
  };
  return (
    <button
      className={classNames(
        'btn',
        className,
        background,
        color,
        { [borderColor[background]]: background },
        'border flex items-center px-6 py-3 rounded-sm  hover:text-white',
        fontSize,
        'hover:brightness-95',
      )}
      type="button"
    >
      {icon && icon}
      <div>{children}</div>
    </button>
  );
}

export default Button;
