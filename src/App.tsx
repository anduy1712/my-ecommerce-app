import React from 'react';
import Counter from 'src/components/Count';

function App() {
  return (
    <div className="App">
      <p className="text-2xl">Test tailwind</p>
      <p className="text-2xl">Test tailwind</p>

      <Counter />
    </div>
  );
}

export default App;
