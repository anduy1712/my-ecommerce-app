export const PATH_NAME = {
  CLIENT: {
    HOME: '/',
    DETAIL: '/detail',
  },
  ADMIN: {
    PRODUCTS: '/admin/products',
    CREATE_PRODUCT: '/admin/create-product',
  },
};

export const ANOTHER = {};
