import React, { Suspense } from 'react';
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
// import required modules
import { Autoplay, Pagination, Navigation } from 'swiper';

const Product = React.lazy(() => import('./Product'));

function HomePage() {
  return (
    <>
      <div className="flex mb-4">
        {/* slider */}
        <div className="w-[70%] mr-1">
          <Swiper
            spaceBetween={30}
            centeredSlides
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            pagination={{
              clickable: true,
            }}
            navigation
            modules={[Autoplay, Pagination, Navigation]}
            className="mySwiper"
          >
            <SwiperSlide>
              <img
                className="w-full"
                src="https://cf.shopee.vn/file/vn-50009109-d6190baf2c743d1ad1be8d2eb78aed8a_xxhdpi"
                alt="slide"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                className="w-full"
                src="https://cf.shopee.vn/file/vn-50009109-65ad65ad36671960700306a2f9d3a445_xxhdpi"
                alt="slide"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img className="w-full" src="https://cf.shopee.vn/file/fa79715264f5c973648d8096a8aa9773_xxhdpi" alt="slide" />
            </SwiperSlide>
          </Swiper>
        </div>
        {/* two banners */}
        <div className="flex flex-col gap-1	w-[30%]">
          <img
            className="h-1/2"
            src="https://cf.shopee.vn/file/vn-50009109-35939e2d7ba53f1551d8762ab87bdd02_xhdpi"
            alt="promotion"
          />
          <img
            className="h-1/2"
            src="https://cf.shopee.vn/file/vn-50009109-efe200f2d32ea5f8d209f7e970c834e2_xhdpi"
            alt="promotion"
          />
        </div>
      </div>
      <div className="mb-6 bg-white">
        <h3 className="px-4 py-5 text-base font-medium uppercase text-zinc-600">Danh Mục</h3>
        <ul className="flex flex-wrap border-t-[1px]  border-zinc-200">
          {Array(20)
            .fill(0)
            .map(() => (
              <li key={`${Math.random()}`} className="card-product w-[10%] text-center">
                <img src="https://cf.shopee.vn/file/687f3967b7c2fe6a134a2c11894eea4b_tn" alt="circle img" />
                <p className="mb-6 text-sm">Lorem ipsum dolor </p>
              </li>
            ))}
        </ul>
      </div>
      <Suspense fallback={<div>Loading...</div>}>
        <Product />
      </Suspense>
    </>
  );
}

export default HomePage;
