import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'src/store/hooks';
import { fetchProduct } from 'src/store/reducers/productSlice';

function Product() {
  const dispatch = useAppDispatch();
  const { data: productData } = useAppSelector((state) => state.product);

  const postSelectedHandler = (title: string) => title.replace(/ /g, '-');

  useEffect(() => {
    dispatch(fetchProduct());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="z-10">
      <div className="md:container md:mx-auto">
        <div className="bg-white text-center shadow-md mb-2 py-4 text-nc-primary sticky top-[110px] after:absolute after:w-full after:h-1 after:bg-nc-primary after:left-0 after:bottom-0">
          <h3>GỢI Ý HÔM NAY</h3>
        </div>
        <div className="flex flex-wrap -mx-1.5">
          {productData.map((product) => (
            <Link to={`${postSelectedHandler(product.name)}&id=${product.id}`} key={product.id} className="w-1/6 px-1.5  ">
              <div className="bg-white mb-2 overflow-hidden font-light rounded-sm shadow-md hover:border hover:border-nc-primary hover:cursor-pointer">
                <img className="min-h-[188px] object-cover" src={product.images} alt="img card" />
                <div className="px-2 py-2 text-x">
                  <h4 className="h-[48px] overflow-hidden mb-5 font-normal text-black">{product.name}</h4>
                  <div className="flex items-center justify-between">
                    <p className="text-nc-primary">{product.price}</p>
                    <span className="text-[10px] ">Da Ban {product.amount}k</span>
                  </div>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Product;
