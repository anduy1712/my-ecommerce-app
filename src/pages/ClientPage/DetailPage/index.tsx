import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';

import { FreeMode, Navigation, Thumbs } from 'swiper';
import { AiFillStar, AiOutlineMinus, AiOutlinePlus, AiOutlineStar } from 'react-icons/ai';
import Button from 'src/components/Button';
import { BsCartPlus } from 'react-icons/bs';

function SlideImage() {
  return (
    <>
      <Swiper
        style={
          {
            '--swiper-navigation-color': '#fff',
            '--swiper-pagination-color': '#fff',
          } as React.CSSProperties
        }
        spaceBetween={10}
        navigation
        modules={[FreeMode, Navigation, Thumbs]}
        className="mySwiper2"
      >
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-1.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-2.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-3.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-4.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-5.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-6.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-7.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-8.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-9.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-10.jpg" />
        </SwiperSlide>
      </Swiper>
      <Swiper
        spaceBetween={10}
        slidesPerView={4}
        freeMode
        watchSlidesProgress
        modules={[FreeMode, Navigation, Thumbs]}
        className="mySwiper"
      >
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-1.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-2.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-3.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-4.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-5.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-6.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-7.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-8.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-9.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <img alt="slide_image" src="https://swiperjs.com/demos/images/nature-10.jpg" />
        </SwiperSlide>
      </Swiper>
    </>
  );
}

function DetailPage() {
  return (
    <>
      <div className="detail-page w-full flex bg-white border border-[#e9ebec] rounded-sm mb-4 px-3 py-3">
        {/* Slide */}
        <div className="w-4/12 mr-4 ">
          <SlideImage />
        </div>
        {/* Content */}
        <div className="w-8/12 px-2 py-2 ">
          <h3 className="text-[22px] mb-2">Băng keo nano hai mặt trong suốt dài 1m siêu chắc chắn</h3>
          <div className="flex mb-4">
            <div className="flex items-center text-nc-primary text-[20px] relative pr-4 after:content-[''] after:h-full after:bg-[#767676] after:right-0 after:absolute after:top-0 after:w-[0.3px] after:bottom-0">
              <div className="text-[20px] text-nc-primary border-b-2 border-nc-primary mr-1">4.4</div>
              <div className="flex">
                <AiFillStar />
                <AiFillStar />
                <AiFillStar />
                <AiFillStar />
                <AiOutlineStar />
              </div>
            </div>
            <div className="text-[16px] text-[#767676] ml-2 relative px-4 after:content-[''] after:h-full after:bg-[#767676] after:right-0 after:absolute after:top-0 after:w-[0.3px] after:bottom-0">
              <span className="text-[18px] text-black border-b-2 border-black">501</span> Đánh Giá
            </div>
            <div className="text-[16px] text-[#767676] ml-2 px-4">
              <span className="text-[18px] text-black border-b-2 border-black">2.6k</span> Đã Bán
            </div>
          </div>
          <div className="flex items-center w-full bg-[#fafafa] px-4 py-4 mb-4">
            <div className="text-[28px] text-nc-primary mr-4">₫9.000</div>
            <div className="text-[12px] bg-nc-primary text-white px-1 rounded-sm font-bold">50% GIẢM</div>
          </div>
          <div className="flex items-center px-4 mb-8">
            <div className="text-[16px] mr-8 text-[#767676]">Số Lượng</div>
            <div className="flex items-center mr-4 border ">
              <div className="px-3 py-3 text-gray-500">
                <AiOutlineMinus />
              </div>
              <input
                className="h-[40px] w-[60px] outline-none px-2 text-center border-l-[1px] border-r-[1px]"
                type="number"
                value={1}
              />
              <div className="px-3 py-3 text-gray-500">
                <AiOutlinePlus />
              </div>
            </div>
            <div className="text-[16px] text-[#767676]">49209 sản phẩm có sẵn</div>
          </div>
          <div className="flex px-4">
            <Button background="bg-nc-primary" color="text-white" fontSize="text-[18px]">
              Mua Ngay
            </Button>
            <Button
              background="bg-[#ffeee8]"
              color="text-nc-primary"
              fontSize="text-[18px]"
              icon={<BsCartPlus className="mr-2" />}
            >
              Thêm Vào Giỏ Hàng
            </Button>
          </div>
        </div>
      </div>
      <div className="detail-page w-full flex bg-white border border-[#e9ebec] rounded-sm mb-4 px-3 py-3">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis vel expedita possimus doloremque similique, dolorum,
        qui perferendis ducimus vero placeat ex pariatur mollitia tenetur culpa quisquam eveniet reprehenderit alias velit
        repellat nisi. Repellat voluptatum molestias, voluptas sed quisquam ratione nostrum dicta aperiam a laudantium ullam
        fugit nisi quidem reiciendis possimus?
      </div>
    </>
  );
}

export default DetailPage;
