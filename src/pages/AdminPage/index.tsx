import Tippy from '@tippyjs/react';
import React, { useState } from 'react';
import { AiOutlineDashboard, AiOutlineAppstore, AiOutlineSearch, AiOutlinePlusCircle } from 'react-icons/ai';
import { IoIosArrowRoundForward } from 'react-icons/io';
import { MdOutlineSort } from 'react-icons/md';
import { Link, Outlet } from 'react-router-dom';
import Button from 'src/components/Button';
import Category, { ICategoryData } from 'src/components/Category';
import { PATH_NAME } from 'src/const';

const categoryData: ICategoryData[] = [
  {
    id: 1,
    title: 'menu',
    data: [
      {
        id: 1,
        title: 'Ecommerce',
        icon: <AiOutlineDashboard className="mr-2 text-xl" />,
        subCategory: [
          {
            title: 'Products',
            label: 'normal',
            url: '',
            path: PATH_NAME.ADMIN.PRODUCTS,
          },
          {
            title: 'Create Product',
            label: 'new',
            url: '',
            path: PATH_NAME.ADMIN.CREATE_PRODUCT,
          },
          {
            title: 'Order',
            label: 'new',
            url: '',
            path: '/order',
          },
        ],
      },
      {
        id: 2,
        title: 'Apps',
        icon: <AiOutlineAppstore className="mr-2 text-xl" />,
        subCategory: [
          {
            title: 'Analyst',
            label: 'normal',
            url: '',
            path: '/order-1',
          },
          {
            title: 'CRM',
            label: 'new',
            url: '',
            path: '/order-2',
          },
          {
            title: 'Ecommerce',
            label: 'new',
            url: '',
            path: '/order-3',
          },
        ],
      },
    ],
  },
  {
    id: 2,
    title: 'pages',
    data: [
      {
        id: 1,
        title: 'Authentication',
        icon: <AiOutlineDashboard className="mr-2 text-xl" />,
        subCategory: [
          {
            title: 'Analyst',
            label: 'normal',
            url: '',
            path: '/order-4',
          },
          {
            title: 'CRM',
            label: 'new',
            url: '',
            path: '/order-5',
          },
          {
            title: 'Ecommerce',
            label: 'new',
            url: '',
            path: '/order-6',
          },
        ],
      },
      {
        id: 2,
        title: 'Pages',
        icon: <AiOutlineAppstore className="mr-2 text-xl" />,
        subCategory: [
          {
            title: 'Analyst',
            label: 'normal',
            url: '',
            path: '/order',
          },
          {
            title: 'CRM',
            label: 'new',
            url: '',
            path: '/order',
          },
          {
            title: 'Ecommerce',
            label: 'new',
            url: '',
            path: '/order',
          },
        ],
      },
    ],
  },
];

function LoginDropDown() {
  return (
    <div className="bg-white min-w-[180px] shadow-lg rounded-md ">
      <p className="px-4 py-2 text-xs">Welcome Duy An</p>
      <ul className="border-b border-slate-300">
        <li className="flex items-center px-4 py-2 mb-2 text-sm hover:bg-slate-200">
          <AiOutlineDashboard className="mr-2" />
          <Link to="/">Profile</Link>
        </li>
        <li className="flex items-center px-4 py-2 mb-2 text-sm hover:bg-slate-200">
          <AiOutlineDashboard className="mr-2" />

          <Link to="/">Logout</Link>
        </li>
      </ul>
      <ul>
        <li className="flex items-center px-4 py-2 mb-2 text-sm hover:bg-slate-200">
          <AiOutlineDashboard className="mr-2" />
          <Link to="/">Profile</Link>
        </li>
        <li className="flex items-center px-4 py-2 mb-2 text-sm hover:bg-slate-200">
          <AiOutlineDashboard className="mr-2" />
          <Link to="/">Logout</Link>
        </li>
      </ul>
    </div>
  );
}

function AdminPage({ isNavbar = true }: { isNavbar?: boolean }) {
  const [visible, setVisible] = useState(false);
  const show = () => setVisible(true);
  const hide = () => setVisible(false);
  return (
    <div className="flex h-screen admin-page font-poppins">
      {/* SIDEBAR  */}
      <section className="w-[250px] fixed top-0 bottom-0 bg-nc-primary text-gray-200">
        <div className="w-full px-4 py-4 text-center">
          <p className="text-3xl">LOGO</p>
        </div>
        <div className="h-full px-4 overflow-y-scroll dashboard-scrollbar">
          {categoryData.map((category) => (
            <Category key={category.id} title={category.title} data={category.data} />
          ))}
        </div>
      </section>
      <section className="w-full ml-[250px] bg-slate-100">
        {/* NAVBAR */}
        <div className="w-[calc(100%_-_250px)] h-[60px] fixed bg-white shadow-md flex justify-between">
          <div className="flex items-center w-1/2 px-4 py-4">
            <div className="mr-3">
              <MdOutlineSort size={28} />
              {false && <IoIosArrowRoundForward size={28} />}
            </div>
            <div className="flex items-center px-2 py-2 rounded-md bg-slate-200">
              <AiOutlineSearch size={18} className="mr-2" />
              <input className="text-sm outline-none bg-slate-200" type="text" placeholder="Search..." name="" id="" />
            </div>
          </div>
          <div className="flex justify-end w-1/2">
            <Tippy interactive content={<LoginDropDown />} visible={visible} onClickOutside={hide}>
              <div
                aria-hidden="true"
                onClick={visible ? hide : show}
                className="flex items-center gap-3 px-4 py-4 mr-4 bg-slate-200 hover:cursor-pointer"
              >
                <img
                  className="w-[32px] h-[32px] rounded-full"
                  src="https://themesbrand.com/velzon/html/default/assets/images/users/avatar-1.jpg"
                  alt="avatar"
                />
                <div>
                  <p className="text-sm font-bold">Duy An</p>
                  <p className="text-xs text-slate-400">Software engineer</p>
                </div>
              </div>
            </Tippy>
          </div>
        </div>
        {/* CONTENT */}
        <div className="mt-[60px] px-6 py-4 bg-slate-100 h-full">
          {isNavbar && (
            <div className="flex items-center mb-4">
              <div className="flex-1">
                <h3 className="font-bold text-neutral-700">Good Morning, Duy An!</h3>
                <p className="font-light text-neutral-500">Heres whats happening with your store today.</p>
              </div>
              <div className="flex items-center">
                <div className="mr-4">Calender here</div>
                <div>
                  <Button background="bg-[#e0ecef]" color="text-[#0ab39c]" icon={<AiOutlinePlusCircle className="mr-1" />}>
                    Add Product
                  </Button>
                </div>
              </div>
            </div>
          )}
          <Outlet />
        </div>
      </section>
    </div>
  );
}

export default AdminPage;
