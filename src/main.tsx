import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { store } from 'src/store';
import Route from './routes';

import './index.css';
import './App.css';
import './assets/scss/index.scss';
// eslint-disable-next-line import/no-extraneous-dependencies
// import 'tippy.js/dist/tippy.css';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  // <React.StrictMode>
  <Provider store={store}>
    <Route />
  </Provider>,
  // </React.StrictMode>,
);
