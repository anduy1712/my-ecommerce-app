/* eslint-disable no-param-reassign */
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from 'src/store';
import productApi from 'src/api/product';

export type Product = {
  id: number;
  name: string;
  price: number;
  images: string;
  description: string;
  amount: number;
};

export interface ProductState {
  data: Product[];
  status: 'success' | 'loading' | 'failed';
}

const initialState: ProductState = {
  data: [],
  status: 'loading',
};

export const fetchProduct = createAsyncThunk('product/fetchProduct', async () => {
  const response = await productApi.get();
  return response.data;
});

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    increment: (state) => {
      state.data = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProduct.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchProduct.fulfilled, (state, action) => {
        state.status = 'success';
        state.data = action.payload;
      });
  },
});

export const { increment } = productSlice.actions;

export const selectProduct = (state: RootState) => state.product.data;

export default productSlice.reducer;
