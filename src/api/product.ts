import { Product } from 'src/store/reducers/productSlice';
import axiosClient from './axiosClient';

const productApi = {
  get: () => {
    const url = '/products';
    return axiosClient.get<Product[]>(url);
  },
};

export default productApi;
