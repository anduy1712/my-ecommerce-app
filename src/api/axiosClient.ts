import axios from 'axios';
import qs from 'query-string';

const axiosClient = axios.create({
  baseURL: import.meta.env.VITE_API_LOCAL,
  headers: {
    'Content-Type': 'application/json',
  },
  paramsSerializer: {
    encode: (params) => qs.stringify(params),
  },
});

axiosClient.interceptors.response.use(
  (res) => res,
  (err) => {
    throw err;
  },
);

export default axiosClient;
