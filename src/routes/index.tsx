import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Layout from 'src/components/Layout';
import { PATH_NAME } from 'src/const';
import AdminPage from 'src/pages/AdminPage';
import Product from 'src/pages/AdminPage/Product';
import DetailPage from 'src/pages/ClientPage/DetailPage';
import ErrorPage from 'src/pages/ClientPage/ErrorPage';
import HomePage from 'src/pages/ClientPage/HomePage';

const router = createBrowserRouter([
  {
    element: <Layout />,
    children: [
      {
        path: PATH_NAME.CLIENT.HOME,
        element: <HomePage />,
      },
      {
        path: PATH_NAME.CLIENT.DETAIL,
        element: <DetailPage />,
      },
    ],
    errorElement: <ErrorPage />,
  },
  {
    element: <AdminPage />,
    children: [
      {
        path: PATH_NAME.ADMIN.PRODUCTS,
        element: <Product />,
      },
      {
        path: PATH_NAME.ADMIN.CREATE_PRODUCT,
        element: <Product />,
      },
    ],
  },
]);

function Route() {
  return <RouterProvider router={router} />;
}

export default Route;
